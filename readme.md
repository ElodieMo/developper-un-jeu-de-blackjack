Développer un jeu de Blackjack 


Contexte du projet

Le jeu demandera une entrée au joueur (ex : prenez une carte) et lui montrera le résultat (ex : votre score est 17). 

​ 

Après une partie, le jeu vous demandera de quitter ou de redémarrer. 

​ 

N'oubliez pas de stocker votre jeu sur Gitlab ! 

​ 

Objet du jeu

​ 

Le joueur tente de battre le croupier en obtenant un compte aussi proche que possible de 21, sans dépasser 21. 

​ 

Valeurs des cartes : 

    un as vaut 1 ou 11, au choix du joueur
    les cartes faciales valent 10
    toute autre carte (2 à 10) valant sa valeur correspondante

​ 

Le jeu

​ 

Le croupier donne deux cartes face visible au joueur. 

Le croupier se donne une carte face visible et une carte face cachée. 

Le joueur doit décider s'il demande une autre carte pour tenter de se rapprocher du 21 ou s'il s'arrête. 

Jusqu'à ce que le score du joueur soit de 21 ou moins, il peut décider de demander des cartes supplémentaires, une à la fois. Si le score du joueur est supérieur à 21, le croupier gagne et le jeu s'arrête. 

​ 

Le jeu du croupier

​ 

Le croupier retournera sa carte face cachée 

​ 

si le total est de 17 ou plus, il doit s'arrêter. 

si le total est de 16 ou moins, il doit prendre une nouvelle carte. 

si le croupier a un as et que le compter comme 11 porterait le total à 17 ou plus (mais pas plus de 21), le croupier doit compter l'as comme 11 et s'arrêter. 

​ 

Si le score du croupier est supérieur à 21, le joueur gagne. 

​ 

Gagnant

​ 

Si ni le joueur ni le croupier n'ont plus de 21, celui qui a le plus gros score l'emporte. 

​ 

Des astuces

​ 

    Ne créez pas une pile de cartes (un tableau de cartes), choisissez simplement des valeurs aléatoires entre 1 et 10
    Gardez-le aussi simple que possible
    Travailler avec de petites étapes incrémentielles

